package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialNavigationDrawer()

    }

    private fun initialNavigationDrawer() {
        bottomNavigation.setOnNavigationItemSelectedListener { item: MenuItem ->
            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.item_home -> {
                    showHomePage()
                    true
                }
                R.id.item_history -> {
                    showHistoryPage()
                    true
                }
                R.id.item_point -> {
                    showPointPage()
                    true
                }
                R.id.item_reward -> {
                    showRewardPage()
                    true
                }
                else -> false
            }
        }
    }

    private fun showHomePage() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(
            R.id.mainFrame,
            HomeFragment()
        )
        transaction.commit()
    }

    private fun showPointPage() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(
            R.id.mainFrame,
            PointFragment()
        )
        transaction.commit()
    }

    private fun showHistoryPage() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(
            R.id.mainFrame,
            HistoryFragment()
        )
        transaction.commit()
    }

    private fun showRewardPage() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(
            R.id.mainFrame,
            RewardFragment()
        )
        transaction.commit()
    }
}